﻿using Mechanics;
using UnityEngine;

namespace Data
{
    public enum PieceType
    {
        // None,
        Bishop, // elephant
        King,
        Knight, // horse
        Pawn, // пешка
        Queen,
        Rook // ладья
    }

    public enum PieceColor : byte
    {
        // None,
        White,
        Black
    }

    [CreateAssetMenu(fileName = "New Piece", menuName = "Data/Piece")]
    public class Piece : ScriptableObject, IPiece
    {
        [SerializeField] private PieceType pieceType;
        [SerializeField] private PieceColor pieceColor;

        public PieceColor Color
        {
            get => pieceColor;
            set => pieceColor = value;
        }

        public PieceType Type
        {
            get => pieceType;
            set => pieceType = value;
        }
    }
}