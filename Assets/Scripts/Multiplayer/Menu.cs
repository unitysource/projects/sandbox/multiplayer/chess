using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class Menu : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_Text logText;
    [SerializeField] private string sceneToLoad;

    private void Start()
    {
        PhotonNetwork.NickName = $"Player {Random.Range(10, 1000)}";
        Log($"Player's name is set to: [{PhotonNetwork.NickName}]");

        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Log("Connected to Master Server");
    }

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(null, new RoomOptions {MaxPlayers = 2});
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        Log($"Joined to room");
        
        PhotonNetwork.LoadLevel(sceneToLoad);
    }

    private void Log(string massage)
    {
        Debug.Log(massage);
        logText.text += $"\n {massage}";
    }
}