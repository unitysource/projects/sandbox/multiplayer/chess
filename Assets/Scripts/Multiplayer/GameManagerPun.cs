﻿using System;
using ExitGames.Client.Photon;
using Mechanics;
using Mechanics.Piece_Stuff;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Multiplayer
{
    public class GameManagerPun : MonoBehaviourPunCallbacks
    {
        [SerializeField] private GameObject piecePrefabBlack;
        [SerializeField] private GameObject piecePrefabWhite;

        private void Start()
        {
            InitPiece(piecePrefabBlack);
            InitPiece(piecePrefabWhite);
            
            PhotonPeer.RegisterType(typeof(Vector2Int), 212, SerializeVector2Int, DeserializeVector2Int);
        }

        private void InitPiece(GameObject piecePrefab)
        {
            Cell randomCell = SceneGrid.Instance.GetRandomCell();
            GameObject king =
                PhotonNetwork.Instantiate($"Pieces/{piecePrefab.name}",
                    randomCell.WorldPosition, Quaternion.identity);
            king.GetComponent<PieceModel>().Cell = randomCell;
        }

        public void LeaveBtn()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnLeftRoom()
        {
            // When we leave Room
            SceneManager.LoadScene(0);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log($"Player {newPlayer.NickName} entered room");
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log($"Player {otherPlayer.NickName} left room");
        }

        public static object DeserializeVector2Int(byte[] data)
        {
            Vector2Int result = new Vector2Int {
                x = BitConverter.ToInt32(data, 0),
                y = BitConverter.ToInt32(data, 4)
            };


            return result;
        }

        public static byte[] SerializeVector2Int(object obj)
        {
            Vector2Int vector = (Vector2Int) obj;
            byte[] result = new byte[0];

            BitConverter.GetBytes(vector.x).CopyTo(result, 0);
            BitConverter.GetBytes(vector.y).CopyTo(result, 4);

            return result;
        }
    }
}