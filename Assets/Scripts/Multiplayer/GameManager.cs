﻿using System;
using ExitGames.Client.Photon;
using Mechanics;
using Mechanics.Piece_Stuff;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Multiplayer
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] piecePrefabs;

        private void Start()
        {
            foreach (var piecePrefab in piecePrefabs)
                InitPiece(piecePrefab);
        }

        private void InitPiece(GameObject piecePrefab)
        {
            Cell randomCell = SceneGrid.Instance.GetRandomCell();
            GameObject king =
                Instantiate(piecePrefab,
                    randomCell.WorldPosition, Quaternion.identity);

            PieceModel model = king.GetComponent<PieceModel>();
            model.Cell = randomCell;
            SceneGrid.Instance.SetCellPiece(randomCell.RelativePosition, model);
        }
    }
}