﻿using System;
using Tools;
using UnityEngine;

namespace Mechanics
{
    public class InputManager : MonoBehaviour
    {
        private Camera _camera;

        public static Vector2 SelectedPoint { get; set; }

        private void Start()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            UpdateSelection();
        }

        private void UpdateSelection()
        {
            if (!_camera) return;

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var raycastHit, 40f,
                    LayerMask.GetMask(Layers.Board)))
                    SelectedPoint = new Vector2(raycastHit.point.x, raycastHit.point.z);
                else
                    SelectedPoint = Vector2.one * -1f;
            }

        }
    }
}