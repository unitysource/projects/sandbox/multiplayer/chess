﻿using System;
using Mechanics.Piece_Stuff;
using UnityEngine;

namespace Mechanics
{
    public class PieceMoveController : MonoBehaviour
    {
        private PieceModel _pieceModel;

        private void Start()
        {
            _pieceModel = GetComponent<PieceModel>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) && _pieceModel.IsActive && Input.GetKey(KeyCode.LeftShift))
            {
                // Debug.Log($"here");
                Vector2Int? relativePosition =
                    SceneGrid.Instance.AbsoluteToRelativePosition(InputManager.SelectedPoint);
                foreach (Cell cell in _pieceModel.AvailableCells)
                {
                    if (cell.RelativePosition == relativePosition)
                    {
                        _pieceModel.Move(cell.WorldPosition);
                        break;
                    }
                }

                DiacivateModel();
            }
        }

        private void OnMouseDown()
        {
            SceneGrid.Instance.StoreTips();

            SceneGrid grid = SceneGrid.Instance;
            PieceModel activePiece = grid.GetActivePiece();

            Debug.Log($"here");

            if (activePiece != null)
            {
                Debug.Log($"here 1");

                activePiece.IsActive = false;
                
                if (activePiece == _pieceModel)
                {
                    Debug.Log($"here 2");
                    DiacivateModel();
                }
            }
            if(activePiece != _pieceModel)
            {
                _pieceModel.IsActive = true;
                
                // Debug.Log(_pieceModel.AvailableCells.Length+" len");
                _pieceModel.AvailableCells = _pieceModel.DetectAvailableCells();
                SceneGrid.Instance.SetTips(_pieceModel);

                for (var i = 0; i < _pieceModel.AvailableCells.Length; i++)
                    Debug.Log($"availableCell {i} = {_pieceModel.AvailableCells[i].RelativePosition}");
            }
        }

        private void DiacivateModel()
        {
            // Array.Clear(_pieceModel.AvailableCells, 0, _pieceModel.AvailableCells.Length);
            _pieceModel.AvailableCells = new Cell[0];
            _pieceModel.IsActive = false;
            SceneGrid.Instance.StoreTips();
            
            Debug.Log("_pieceModel.AvailableCells = " + _pieceModel.AvailableCells.Length);
        }
    }
}