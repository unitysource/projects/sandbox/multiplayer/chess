﻿using Data;
using Mechanics.Piece_Stuff;

namespace Mechanics
{
    public interface IPiece
    {
        PieceColor Color { get; }
        PieceType Type { get; }
        // PieceModel Model { get; } 
    }
}