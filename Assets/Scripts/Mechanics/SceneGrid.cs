﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Mechanics.Piece_Stuff;
using UnityEngine;

namespace Mechanics
{
    // [RequireComponent(typeof(Grid))]
    public class SceneGrid : Grid
    {
        private readonly List<PieceModel> _models = new List<PieceModel>(32);

        [SerializeField] private float cellSize = 1.5f;
        [SerializeField] private float yOffset = 0.17f;

        [Tooltip("For custom coordinate system")] [SerializeField]
        private Vector2 startPoint;

        [SerializeField] private GameObject toolTip;
        [SerializeField] private int tipsPoolLength;
        
        private readonly Queue<GameObject> _tips = new Queue<GameObject>();
        private readonly List<GameObject> _tipsUsable = new List<GameObject>();

        public float CellSize => cellSize;
        public float YOffset => yOffset;

        private float _halfCellSize;

        public static SceneGrid Instance;
        [SerializeField] private Transform tipsContainer;

        private void Awake()
        {
            Instance = this;
            InitGrid();
        }

        private void Start()
        {
            for (int i = 0; i < tipsPoolLength; i++)
            {
                GameObject newTip = Instantiate(toolTip, tipsContainer);
                newTip.SetActive(false);
                _tips.Enqueue(newTip);
            }
        }

        public void SetTips(PieceModel model)
        {
            foreach (Cell cell in model.AvailableCells)
            {
                GameObject newTip = _tips.Dequeue();
                newTip.transform.position = cell.WorldPosition;
                newTip.SetActive(true);
                _tipsUsable.Add(newTip);
            }
        }

        public void StoreTips()
        {
            foreach (GameObject tip in _tipsUsable) 
            {
                tip.SetActive(false);
                _tips.Enqueue(tip);
            }
            _tipsUsable.Clear();
        }

        public PieceModel GetActivePiece() =>
            _models.FirstOrDefault(model => model.IsActive);

        private void InitGrid()
        {
            _halfCellSize = cellSize * 0.5f;
            // Indexing all cells
            EachCellCenterPosition((center, pos) =>
            {
                // PieceColor cellColor = (PieceColor) ((pos.x + pos.y) % 2);
                base[pos] = new Cell(pos, center, null);
            });
        }

        private void OnDrawGizmosSelected()
        {
            InitGrid();
            EachCellCenterPosition((center, pos) =>
            {
                // Gizmos.color = this[pos].Piece.Color is PieceColor.White ? Color.white : Color.black;
                Gizmos.DrawWireCube(center, new Vector3(_halfCellSize, 0, _halfCellSize));
            });

            CurrentPosition(InputManager.SelectedPoint);

            // foreach (Cell cell in _models[0].AvailableCells)
            // {
            //     CurrentPosition(new Vector2(cell.WorldPosition.x, cell.WorldPosition.z));
            // }
        }


        private void CurrentPosition(Vector2 mousePos)
        {
            EachCellCenterPosition((center, pos) =>
            {
                if (Vector2.Distance(mousePos, new Vector2(center.x, center.z)) < _halfCellSize)
                {
                    Gizmos.color = Color.cyan;
                    Gizmos.DrawWireCube(center, new Vector3(_halfCellSize, 0, _halfCellSize));
                }
            });
        }

        public Vector2Int? AbsoluteToRelativePosition(Vector2 mousePos)
        {
            for (int x = 0; x < Cells.GetLength(0); x++)
            for (int y = 0; y < Cells.GetLength(1); y++)
            {
                Vector3 center = GetCenterPosition(x, y);
                if (Vector2.Distance(mousePos, new Vector2(center.x, center.z)) < _halfCellSize)
                    return new Vector2Int(x, y);
            }

            return null;
        }

        public void SetCellPiece(Vector2Int relPos, PieceModel piece)
        {
            if (piece != null)
            {
                SetAbstractCellPiece(relPos, piece.piece);
                piece.Cell = this[relPos];

                if (!_models.Contains(piece))
                {
                    // Debug.Log($"piece: {piece.piece.Type}");
                    _models.Add(piece);
                }
            }
            else
                SetAbstractCellPiece(relPos, null);
        }

        public PieceModel GetCellPiece(Vector2Int relPos)
        {
            foreach (PieceModel model in _models)
                if (model.Cell.RelativePosition == relPos)
                {
                    Debug.Log($"here");
                    return model;
                }

            return null;
        }

        private void EachCellCenterPosition(Action<Vector3, Vector2Int> action)
        {
            for (int x = 0; x < Cells.GetLength(0); x++)
            for (int y = 0; y < Cells.GetLength(1); y++)
                action(GetCenterPosition(x, y), new Vector2Int(x, y));
        }

        private Vector3 GetCenterPosition(int x, int y)
        {
            return new Vector3
            {
                x = startPoint.x + x * cellSize + _halfCellSize,
                y = yOffset,
                z = startPoint.y + y * cellSize + _halfCellSize
            };
        }

        public Cell[] GetCellsByDistance(Vector2Int relPos, int relDistByX, int relDistByY)
        {
            List<Cell> cells = new List<Cell>();
            EachCellCenterPosition((center, pos) =>
            {
                int xAbs = Mathf.Abs(relPos.x - pos.x);
                int yAbs = Mathf.Abs(relPos.y - pos.y);
                if (xAbs == relDistByX && yAbs == relDistByY)
                {
                    cells.Add(base[pos]);
                }
            });

            return cells.ToArray();
        }
    }
}