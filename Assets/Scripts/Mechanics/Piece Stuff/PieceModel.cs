﻿using Data;
using UnityEngine;

namespace Mechanics.Piece_Stuff
{
    public abstract class PieceModel : MonoBehaviour
    {
        public Piece piece;
        public Cell Cell;
        public bool IsActive { get; set; }

        public Cell[] AvailableCells { get; set; } = new Cell[0];

        public abstract void Move(Vector3 worldPos);
        public abstract Cell[] DetectAvailableCells();
    }
}