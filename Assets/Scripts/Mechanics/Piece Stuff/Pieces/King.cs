﻿using System.Collections.Generic;
using Data;
using Multiplayer;
using UnityEngine;

namespace Mechanics.Piece_Stuff.Pieces
{
    public class King : PieceModel
    {
        public override void Move(Vector3 worldPos)
        {
            SceneGrid sceneGrid = SceneGrid.Instance;
            Vector2Int? relativePosition = sceneGrid.AbsoluteToRelativePosition(
                new Vector2(worldPos.x, worldPos.z));

            if (relativePosition != null)
            {
                PieceModel cellPiece = sceneGrid.GetCellPiece((Vector2Int) relativePosition);
                if (cellPiece != null)
                    cellPiece.gameObject.SetActive(false);

                sceneGrid.SetCellPiece(
                    (Vector2Int) relativePosition, this);

                sceneGrid.SetCellPiece(
                    Cell.RelativePosition, null);
            }

            while (Vector3.Distance(transform.position, worldPos) > 0.1f)
                transform.position =
                    Vector3.Lerp(transform.position, worldPos, Time.deltaTime * 3f);
        }

        public override Cell[] DetectAvailableCells()
        {
            List<Cell> cells = new List<Cell>();

            Vector2Int currentRelPos = Cell.RelativePosition;
            SceneGrid sceneGrid = SceneGrid.Instance;

            // Cell selectedCell = sceneGrid.SelectedCell;
            // Vector2Int relPos = selectedCell.RelativePosition;

            //Find all available cells by piece movement type
            cells.AddRange(sceneGrid.GetCellsByDistance(currentRelPos, 1, 0));
            cells.AddRange(sceneGrid.GetCellsByDistance(currentRelPos, 0, 1));
            cells.AddRange(sceneGrid.GetCellsByDistance(currentRelPos, 1, 1));

            //Check cells by their piece type
            foreach (Cell cell in cells.ToArray())
            {
                // if (sceneGrid.IsPieceNull(cell.RelativePosition))
                // {
                //     
                // }
                if (!sceneGrid.IsPieceNull(cell.RelativePosition))
                {
                    // KilledPiecesContainer.AddKilledPiece(true, pieceInCell);

                    if (cell.Piece.Type == PieceType.King || cell.Piece.Color == piece.Color)
                        cells.Remove(cell);
                }
            }

            return cells.ToArray();
        }
    }
}