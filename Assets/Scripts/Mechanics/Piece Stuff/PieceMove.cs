﻿using System;
using UnityEngine;

namespace Mechanics.Piece_Stuff
{
    public abstract class PieceMove : MonoBehaviour
    {
        private void OnMouseDown()
        {
            Debug.Log("Mouse Down");
        }

        private void OnMouseUp()
        {
            // Debug.Log(Input.mousePosition);
        }

        public abstract void Move();
    }
}