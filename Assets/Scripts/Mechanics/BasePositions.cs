﻿// using System;
// using System.Collections.Generic;
// using Data;
// using Mechanics.Piece_Stuff;
// using UnityEngine;
//
// namespace Mechanics
// {
//     public class BasePositions : MonoBehaviour
//     {
//         [SerializeField] private GameObject[] pieces;
//         [SerializeField] private Transform board;
//         [SerializeField] private Transform parent;
//
//         [SerializeField] private Grid grid;
//
//
//         private Vector3 _boardPos;
//         private float _curCellForPawn = 3.5f;
//         private float _cellSize;
//
//         private readonly Dictionary<PieceType, int> _pieceCounters = new Dictionary<PieceType, int>
//         {
//             {PieceType.Bishop, 4},
//             {PieceType.Knight, 4},
//             {PieceType.Rook, 4}
//         };
//
//         private void Start()
//         {
//             _boardPos = board.position;
//             _cellSize = grid.CellSize;
//
//             // BasePositionConfig();
//         }
//
//         private void BasePositionConfig()
//         {
//             foreach (var piece in pieces)
//             {
//                 var curPiece = piece.GetComponent<PieceModel>().piece;
//                 var newPeaceObj = Instantiate(piece, parent);
//         
//                 Vector2 newPosition = Vector2.zero;
//                 bool color = curPiece.Color is PieceColor.White;
//         
//                 switch (curPiece.Type)
//                 {
//                     case PieceType.Bishop:
//                         if (_pieceCounters[PieceType.Bishop] > 2)
//                         {
//                             newPosition = color
//                                 ? new Vector2(_cellSize * 1.5f, -_cellSize * 3.5f)
//                                 : new Vector2(_cellSize * 1.5f, _cellSize * 3.5f);
//         
//                             _pieceCounters[PieceType.Bishop]--;
//                         }
//                         else if (_pieceCounters[PieceType.Bishop] <= 2)
//                         {
//                             newPosition = color
//                                 ? new Vector2(-_cellSize * 1.5f, -_cellSize * 3.5f)
//                                 : new Vector2(-_cellSize * 1.5f, _cellSize * 3.5f);
//                         }
//         
//                         break;
//                     case PieceType.King:
//                         newPosition = color
//                             ? new Vector2(_cellSize * 0.5f, -_cellSize * 3.5f)
//                             : new Vector2(_cellSize * 0.5f, _cellSize * 3.5f);
//         
//                         break;
//                     case PieceType.Knight:
//                         if (_pieceCounters[PieceType.Knight] > 2)
//                         {
//                             newPosition = color
//                                 ? new Vector2(_cellSize * 2.5f, _cellSize * -3.5f)
//                                 : new Vector2(_cellSize * 2.5f, _cellSize * 3.5f);
//         
//                             _pieceCounters[PieceType.Knight]--;
//                         }
//                         else if (_pieceCounters[PieceType.Knight] <= 2)
//                         {
//                             newPosition = color
//                                 ? new Vector2(_cellSize * -2.5f, _cellSize * -3.5f)
//                                 : new Vector2(_cellSize * -2.5f, _cellSize * 3.5f);
//                         }
//         
//                         break;
//                     case PieceType.Queen:
//                         newPosition = color
//                             ? new Vector2(-_cellSize * 0.5f, -_cellSize * 3.5f)
//                             : new Vector2(-_cellSize * 0.5f, _cellSize * 3.5f);
//         
//                         break;
//                     case PieceType.Rook:
//                         if (_pieceCounters[PieceType.Rook] > 2)
//                         {
//                             newPosition = color
//                                 ? new Vector2(_cellSize * 3.5f, _cellSize * -3.5f)
//                                 : new Vector2(_cellSize * 3.5f, _cellSize * 3.5f);
//         
//                             _pieceCounters[PieceType.Rook]--;
//                         }
//                         else if (_pieceCounters[PieceType.Rook] <= 2)
//                         {
//                             newPosition = color
//                                 ? new Vector2(_cellSize * -3.5f, _cellSize * -3.5f)
//                                 : new Vector2(_cellSize * -3.5f, _cellSize * 3.5f);
//                         }
//         
//                         break;
//                     case PieceType.Pawn:
//                         var length = 2.5f;
//                         newPosition = color
//                             ? new Vector2(_cellSize * _curCellForPawn--, _cellSize * -length)
//                             : new Vector2(_cellSize * _curCellForPawn, _cellSize * length);
//         
//                         break;
//                     default:
//                         Debug.LogError("What is it?");
//                         throw new ArgumentOutOfRangeException();
//                 }
//         
//                 newPeaceObj.transform.position = _boardPos + new Vector3(newPosition.x, grid.YOffset, newPosition.y);
//             }
//         }
//     }
// }