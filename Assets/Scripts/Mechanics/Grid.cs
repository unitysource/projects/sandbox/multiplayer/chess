﻿using Data;
using Mechanics.Piece_Stuff;
using UnityEngine;

namespace Mechanics
{
    public abstract class Grid : MonoBehaviour
    {
        protected readonly Cell[,] Cells = new Cell[8, 8];
        public Cell SelectedCell { get; private set; }

        protected Cell this[Vector2Int pos]
        {
            get => Cells[pos.x, pos.y];
            set => Cells[pos.x, pos.y] = value;
        }

        public void SetAbstractCellPiece(Vector2Int relPos, Piece piece) =>
            this[relPos].Piece = piece;

        public bool IsPieceNull(Vector2Int relativePos) =>
            this[relativePos].Piece == null;

        public IPiece GetPieceInCell(Vector2Int relativePos) =>
            this[relativePos].Piece;

        public Cell GetRandomCell() => 
            this[new Vector2Int(Random.Range(0, 8), Random.Range(0, 8))];
    }
}