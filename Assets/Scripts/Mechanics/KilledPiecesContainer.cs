﻿using System.Collections.Generic;
using Data;

namespace Mechanics
{
    public static class KilledPiecesContainer
    {
        private static readonly List<IPiece> WhiteKilledPieces = new List<IPiece>();
        private static readonly List<IPiece> BlackKilledPieces = new List<IPiece>();

        public static void AddKilledPiece(bool isWhite, IPiece piece)
        {
            if (isWhite)
                WhiteKilledPieces.Add(piece);
            else
                BlackKilledPieces.Add(piece);
        }

        public static IPiece GetKilledPiece(bool isWhite, PieceType pieceType)
        {
            return isWhite
                ? WhiteKilledPieces.Find(item => item.Type == pieceType)
                : BlackKilledPieces.Find(item => item.Type == pieceType);
        }
    }
}