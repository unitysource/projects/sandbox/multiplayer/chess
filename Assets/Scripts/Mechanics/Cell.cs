﻿using Data;
using Mechanics.Piece_Stuff;
using UnityEngine;

namespace Mechanics
{
    public sealed class Cell
    {
        public Cell(Vector2Int relativePosition, Vector3 worldPosition, IPiece pieceModel)
        {
            Piece = pieceModel;
            RelativePosition = relativePosition;
            WorldPosition = worldPosition;
        }

        public Vector2Int RelativePosition { get; }
        public Vector3 WorldPosition { get; }
        public IPiece Piece { get; set; }
    }
}