﻿using Mechanics.Piece_Stuff;
using Photon.Pun;
using UnityEngine;

namespace Mechanics
{
    public class PieceMoveControllerPun : MonoBehaviour, IPunObservable
    {
        private PhotonView _photonView;
        private PieceModel _pieceModel;

        private bool _isActive;

        private Cell[] _availableCells = new Cell[0];

        private void Start()
        {
            // _meshRenderer = GetComponent<MeshRenderer>();
            _pieceModel = GetComponent<PieceModel>();
            _photonView = GetComponent<PhotonView>();
        }

        private void Update()
        {
            if (_photonView.IsMine)
            {
                if (Input.GetMouseButtonDown(0) && _isActive && Input.GetKey(KeyCode.LeftShift))
                {
                    // Debug.Log($"here");
                    Vector2Int? relativePosition =
                        SceneGrid.Instance.AbsoluteToRelativePosition(InputManager.SelectedPoint);
                    foreach (Cell cell in _availableCells)
                    {
                        if (cell.RelativePosition == relativePosition)
                        {
                            _pieceModel.Move(cell.WorldPosition);
                            break;
                        }
                    }

                    _isActive = false;
                }
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            // if (stream.IsWriting)
            // {
            //     stream.SendNext(_isRed);
            // }
            // else
            //     _isRed = (bool) stream.ReceiveNext();
        }

        private void OnMouseDown()
        {
            _isActive = !_isActive;
            _availableCells = _pieceModel.DetectAvailableCells();
            // Debug.Log("isClickedMe = " + _isActive);
        }
    }
}